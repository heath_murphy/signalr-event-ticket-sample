﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using SignalRTicketSample.Models;

namespace SignalRTicketSample.Hubs
{
    public class TicketAdminHub : Hub
    {
        public override Task OnConnected()
        {
            TicketHub.TicketEngine.ConnectAdmin(Context.ConnectionId);
            return base.OnConnected();
        }

        public void SimulateTimeout(string connectionId)
        {
            TicketHub.TicketEngine.TimeOut(connectionId);
        }
    }
}
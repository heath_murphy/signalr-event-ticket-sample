﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using SignalRTicketSample.Models;

namespace SignalRTicketSample.Hubs
{
    public class TicketHub : Hub
    {
        // In a real world scenario I would probably use a DI container versus this.
        public static TicketEngine TicketEngine { get; set; }

        public override Task OnConnected()
        {
            TicketEngine.ConnectClient(Context.ConnectionId);
            return base.OnConnected();
        }

        public void Register(Guid eventId, int quantity)
        {
            TicketEngine.Register(Context.ConnectionId, quantity);
        }

        public void Complete(Guid registrationId)
        {
            TicketEngine.Complete(registrationId);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SignalRTicketSample.Hubs;
using SignalRTicketSample.Models;

namespace SignalRTicketSample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(GetEvents());
        }

        public ActionResult EventRegistration(Guid id)
        {
            return View(GetEvents().FirstOrDefault(e => e.Id == id));
        }

        public PartialViewResult GetRegistrationInputForm(Guid ticketId)
        {
            var model = TicketHub.TicketEngine.GetTicket(ticketId);
            return PartialView("_RegistrationInputForm", model);
        }

        public ActionResult EventRegistrationCompleted()
        {
            return View();
        }

        public ActionResult TimedOut()
        {
            return View();
        }

        public static List<TickettedEvent> GetEvents()
        {
            return new List<TickettedEvent>
                {
                    new TickettedEvent
                        {
                            Id = new Guid("F9168C5E-CEB2-4faa-B6BF-329BF39FA1E4"),
                            Name = "Dog food Conference",
                            Description =
                                "The Dog Food Conference was founded in 2008, held at the Microsoft and DeVry University building in Columbus, Ohio.",
                            ImageUrl = "~/images/dogfoodimg.png"
                        },
                    new TickettedEvent
                        {
                            Id = new Guid("F9168C5E-CEB2-4faa-FFFA-329BF39FA1E4"),
                            Name = "CodeMash",
                            Description =
                                "CodeMash is a unique event that will educate developers on current practices, methodologies, and technology trends in a variety of platforms and development languages such as Java, .Net, Ruby, Python and PHP.",
                            ImageUrl = "~/images/logo-codemash.png"
                        },
                    new TickettedEvent
                        {
                            Id = new Guid("F9168C5E-CEB2-4faa-BBBB-329BF39FA1E4"),
                            Name = "M3 Conference",
                            Description =
                                "The M3 Conference brings together technology professionals to advance excellence and promote innovation in mobile technology.",
                            ImageUrl = "~/images/m3-logo.png"
                        }
                };
        }
    }
}

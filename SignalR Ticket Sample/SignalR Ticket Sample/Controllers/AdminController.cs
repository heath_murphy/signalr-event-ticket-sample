﻿using System.Linq;
using System.Web.Mvc;
using SignalRTicketSample.Hubs;
using SignalRTicketSample.Models;

namespace SignalRTicketSample.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult StartEvent(int secondsToStart, int quantityAvailable)
        {
            var eventItem = HomeController.GetEvents().First();
            var ticketEngine = TicketEngine.StartEvent(eventItem.Id, secondsToStart, quantityAvailable);
            TicketHub.TicketEngine = ticketEngine;

            return RedirectToAction("Status");
        }

        public ActionResult Status()
        {
            return View();
        }
    }
}

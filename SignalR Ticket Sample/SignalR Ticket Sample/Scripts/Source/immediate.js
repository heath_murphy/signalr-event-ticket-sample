﻿var DC = {};

DC.immediate = (function (Modernizr, DC) {

    var methods = {
        init: function() {
        }
    };

    //  Public methods
    return {
        init: methods.init
    };

}(Modernizr, DC));

DC.immediate.init();
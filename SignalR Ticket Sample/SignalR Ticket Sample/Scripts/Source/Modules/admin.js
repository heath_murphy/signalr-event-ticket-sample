﻿DC.Admin = (function () {
    var methods = {
        init: function () {
        }
    };
    return {
        init: methods.init
    };
}(jQuery, Modernizr, DC));

DC.Admin.Status = (function ($) {
    var methods = {
        init: function () {
            // Connect to ticket hub
            $.connection.hub.start()
                .done(methods.wireUpEvents);

            // client-side rpc wireup
            variables.ticketAdminHub.client.registrationOpen = methods.registrationOpen;
            variables.ticketAdminHub.client.startsIn = methods.startsIn;
            variables.ticketAdminHub.client.reportNewClient = methods.reportNewClient;
            variables.ticketAdminHub.client.newRegistrationHold = methods.newRegistrationHold;
            variables.ticketAdminHub.client.newQueuedUpConnection = methods.newQueuedUpConnection;
            variables.ticketAdminHub.client.completedRegistration = methods.completedRegistration;
            variables.ticketAdminHub.client.removeRegistrationHold = methods.removeRegistrationHold;
            variables.ticketAdminHub.client.removeQueuedUpConnection = methods.removeQueuedUpConnection;
        },
        fadeAllOut: function () {
            variables.spinnerContainer.fadeOut();
            variables.timerContainer.fadeOut();
            variables.reservationContainer.fadeOut();
        },
        startsIn: function (date) {
            methods.fadeAllOut();
            variables.timerContainer.fadeIn();
            variables.registrationStartDate = new Date(date);
            variables.timingInterval = setInterval(function () {
                var remainingMiliseconds = Math.abs(variables.registrationStartDate - new Date());
                variables.remainingSecondsSpan.html(remainingMiliseconds / 1000);
            }, 1000);
            
        },
        simulateTimeout: function (registrationId) {
            return variables.ticketAdminHub.server.simulateTimeout(registrationId);
        },
        reportNewClient: function (connectionId) {
            variables.connectionsList.append("<li id='" + connectionId + "'>ConnectionId: " + connectionId + "</li>");
        },
        newRegistrationHold: function (connectionId, quantity) {
            variables.registrationHoldsList.append("<li id='" + connectionId + "'>ConnectionId: " + connectionId + " - Quantity: " + quantity + " (Click to Force TimeOut) </li>");
        },
        newQueuedUpConnection: function (connectionId, quantity) {
            variables.registrationQueueList.append("<li id='" + connectionId + "'>ConnectionId: " + connectionId + " - Quantity: " + quantity + "</li>");
        },
        completedRegistration: function (connectionId, quantity) {
            variables.completedList.append("<li id='" + connectionId + "'>ConnectionId: " + connectionId + " - Quantity: " + quantity + "</li>");
        },
        removeRegistrationHold: function (connectionId) {
            variables.registrationHoldsList.find("#" + connectionId).remove();
        },
        removeQueuedUpConnection: function (connectionId) {
            variables.registrationQueueList.find("#" + connectionId).remove();
        },
        registrationOpen: function (quantity) {
            methods.fadeAllOut();
            clearInterval(variables.timingInterval);
            variables.reservationContainer.append("<div>Quantity Available: " + quantity + "</div>");
            variables.reservationContainer.fadeIn();
        },
        wireUpEvents: function () {
            variables.registrationHoldsList.on("click", "li", function () {
                methods.simulateTimeout(this.id);
            });
        }
    };
    var variables = {
        ticketAdminHub: $.connection.ticketAdminHub,
        timingInterval: {},
        spinnerContainer: $(".spinnerContainer"),
        reservationContainer: $("#reservationContainer"),
        timerContainer: $("#timerContainer"),
        remainingSecondsSpan: $("#remainingSecondsSpan"),
        connectionsList: $("#connectionsList"),
        registrationHoldsList: $("#registrationHoldsList"),
        registrationQueueList: $("#registrationQueueList"),
        completedList: $("#completedList"),
    };
    return {
        init: methods.init
    };
}(jQuery, Modernizr, DC));
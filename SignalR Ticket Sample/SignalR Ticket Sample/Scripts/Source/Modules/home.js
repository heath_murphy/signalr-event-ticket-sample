﻿DC.Home = (function () {
    var methods = {
        init: function () {
        }
    };
    return {
        init: methods.init
    };
}(jQuery, Modernizr, DC));

DC.Home.EventRegistration = (function ($) {
    var methods = {
        init: function () {
            // Connect to ticket hub
            $.connection.hub.start()
                .done(methods.wireUpEvents);

            // client-side rpc wireup
            variables.ticketHub.client.registrationOpen = methods.registrationOpen;
            variables.ticketHub.client.startsIn = methods.startsIn;
            variables.ticketHub.client.registrationReady = methods.registrationReady;
            variables.ticketHub.client.queuedUp = methods.queuedUp;
            variables.ticketHub.client.outOfTickets = methods.outOfTickets;
            variables.ticketHub.client.timedOut = methods.timedOut;
        },
        fadeAllOut: function() {
            variables.spinnerContainer.fadeOut();
            variables.timerContainer.fadeOut();
            variables.reservationContainer.fadeOut();
            variables.registrationInput.fadeOut();
            variables.queuedUpContainer.fadeOut();
        },
        registrationOpen: function () {
            methods.fadeAllOut();
            clearInterval(variables.timingInterval);
            variables.reservationContainer.fadeIn();
        },
        startsIn: function (date) {
            methods.fadeAllOut();
            variables.timerContainer.fadeIn();
            variables.registrationStartDate = new Date(date);
            variables.timingInterval = setInterval(function () {
                var remainingMiliseconds = Math.abs(variables.registrationStartDate - new Date());
                variables.remainingSecondsSpan.html(remainingMiliseconds / 1000);
            },1000);
        },
        registrationReady: function (ticketId) {
            methods.fadeAllOut();
            variables.registrationInput.load("/Home/GetRegistrationInputForm", { ticketId: ticketId }).fadeIn();
            variables.registrationId = ticketId;
        },
        queuedUp: function () {
            methods.fadeAllOut();
            variables.queuedUpContainer.fadeIn();
        },
        outOfTickets: function () {
            methods.fadeAllOut();
            variables.outOfTicketsContainer.fadeIn();
        },
        register: function (eventId, quantity) {
            return variables.ticketHub.server.register(eventId, quantity);
        },
        complete: function (registrationId) {
            return variables.ticketHub.server.complete(registrationId);
        },
        timedOut: function () {
            window.location = "/Home/TimedOut";
        },
        wireUpEvents : function() {
            variables.reserveTicketBtn.on("click", function () {
                methods.register(variables.eventId, $("#quantity :selected").text());
            });
            variables.registrationInput.on("click", "#completeRegistrationButton", function () {
                methods.complete(variables.registrationId).done(function() {
                    window.location = "/Home/EventRegistrationCompleted";
                });
            });
        }
    };

    var variables = {
        ticketHub: $.connection.ticketHub,
        spinnerContainer: $(".spinnerContainer"),
        reservationContainer: $("#reservationContainer"),
        timerContainer: $("#timerContainer"),
        remainingSecondsSpan: $("#remainingSecondsSpan"),
        reserveTicketBtn: $("#reserveTicketBtn"),
        eventId: $("#eventIdInput").val(),
        registrationId: {},
        registrationInput: $("#registrationInput"),
        queuedUpContainer: $("#queuedUpContainer"),
        outOfTicketsContainer: $("#outOfTicketsContainer"),
        registrationStartDate: {},
        timingInterval: {},
        ticket: {}
    };
    return {
        init: methods.init
    };
}(jQuery));
﻿DC.global = (function ($, Modernizr, DC) {
    var methods = {
        init: function () {
        },
        debug: function (s) {
            if (variables.debugOn) {
                console.log(s);
            }
        }
    };

    var variables = {
        debugOn: false
    };
    return {
        init: methods.init
    };
}(jQuery, Modernizr, DC));
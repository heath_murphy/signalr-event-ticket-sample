﻿DC.init = function () {
    // Set the controller and action names
    var controller = $('body').attr('data-controller'),
        action = $('body').attr('data-action');

    // Fire global functions
    DC.global.init();

    // Check for the existence of objects named after the current controller
    // and action, and fire them if they're defined
    if (typeof DC[controller] !== 'undefined') {
        if (typeof DC[controller][action] !== 'undefined') {
            DC[controller][action].init();
        } else {
            DC[controller].init();
        }
    }
};

$(document).ready(function () {
    DC.init();
});
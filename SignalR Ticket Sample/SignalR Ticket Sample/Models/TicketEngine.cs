﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Microsoft.AspNet.SignalR;
using SignalRTicketSample.Hubs;

namespace SignalRTicketSample.Models
{
    public class TicketEngine
    {
        private readonly IHubContext _ticketHubContext;
        private readonly IHubContext _ticketAdminHubContext;
        private Guid _eventId;
        private DateTime _eventStartTime;
        private Timer _timer;
        private int _quantity;
        private readonly List<Ticket> _registrationHolds;
        private readonly Queue<Tuple<string, int>> _waitQueue;

        private TicketEngine()
        {
            _ticketHubContext = GlobalHost.ConnectionManager.GetHubContext<TicketHub>();
            _ticketAdminHubContext = GlobalHost.ConnectionManager.GetHubContext<TicketAdminHub>();
            _registrationHolds = new List<Ticket>();
            _waitQueue = new Queue<Tuple<string, int>>();
        }

        public static TicketEngine StartEvent(Guid eventId, int secondsToStart, int quantityAvailable)
        {
            // In a real world scenario, I would have used a DI container versus a simple static factory.
            var ticketEngine = new TicketEngine
                {
                    _eventId = eventId,
                    _timer = new Timer(secondsToStart * 1000),
                    _quantity = quantityAvailable
                };

            // Simple timer for a simple demo.
            ticketEngine._timer.Elapsed += (sender, args) =>
                {
                    // Open up registration to all clients! Here comes the flood!
                    ticketEngine._ticketHubContext.Clients.All.registrationOpen();
                    ticketEngine._ticketAdminHubContext.Clients.All.registrationOpen(ticketEngine._quantity);
                    ticketEngine._timer.Enabled = false;
                };

            // Set our timers and start-em up.
            ticketEngine._eventStartTime = DateTime.Now + TimeSpan.FromSeconds(secondsToStart);
            ticketEngine._timer.Start();

            return ticketEngine;
        }

        public void ConnectClient(string connectionId)
        {
            // Tell the admin that a new client has connected.
            _ticketAdminHubContext.Clients.All.reportNewClient(connectionId);

            // See of the event has started.
            if (DateTime.Now >= _eventStartTime)
            {
                // It's started.

                // Are all of the tickets sold out?
                if (_quantity - _registrationHolds.Where(t => t.IsComplete).Select(t => t.Quantity).Sum() > 0)
                {
                    // Nope. Let notify them its ready to go.
                    _ticketHubContext.Clients.Client(connectionId).registrationOpen();
                }
                else
                {
                    // Yes, So Sad.
                    _ticketHubContext.Clients.Client(connectionId).outOfTickets();
                }
            }
            else
            {
                // Not started yet. Notify the client when it will open up.
                _ticketHubContext.Clients.Client(connectionId).startsIn(_eventStartTime);
            }
        }

        public void ConnectAdmin(string connectionId)
        {
            // See of the event has started.
            if (DateTime.Now >= _eventStartTime)
            {
                // It's started.
                _ticketAdminHubContext.Clients.All.registrationOpen(_quantity);
            }
            else
            {
                _ticketAdminHubContext.Clients.All.startsIn(_eventStartTime);
            }
        }

        public Ticket Register(string connectionId, int quantity)
        {
            var ticket = new Ticket
                {
                    RegistrationId = Guid.NewGuid(),
                    ConnectionId = connectionId,
                    EventId = _eventId,
                };

            lock (_registrationHolds)
            {
                // Check to see if we have any left.
                var availableTickets = _quantity - _registrationHolds.Select(t => t.Quantity).Sum();
                if (availableTickets > 0)
                {
                    // We have at least one. We make a command decision and register them for what's left if they are asking for too many.
                    ticket.Quantity = quantity > availableTickets ? availableTickets : quantity;
                    _registrationHolds.Add(ticket);

                    // Notify the lovely client that they are indeed holding a registration ticket! Yes!
                    _ticketHubContext.Clients.Client(ticket.ConnectionId).registrationReady(ticket.RegistrationId);
                    _ticketAdminHubContext.Clients.All.newRegistrationHold(ticket.ConnectionId, ticket.Quantity);
                }
                else
                {
                    // Trouble. We're out of registrations.. Put them in a queue just in case.
                    ticket.Quantity = 0;
                    _waitQueue.Enqueue(new Tuple<string, int>(connectionId, quantity));

                    // Tell the client how sad their predicament really is.
                    _ticketHubContext.Clients.Client(ticket.ConnectionId).queuedUp();
                    _ticketAdminHubContext.Clients.All.newQueuedUpConnection(ticket.ConnectionId, quantity);
                }
            }

            return ticket;
        }

        public void Complete(Guid registrationId)
        {
            // Simulates finalizing a ticket registration. Forever removing it from the world.
            var ticket = _registrationHolds.FirstOrDefault(t => t.RegistrationId == registrationId);
            if (ticket == null) return;

            // That was easy.
            ticket.IsComplete = true;
            _ticketAdminHubContext.Clients.All.completedRegistration(ticket.ConnectionId, ticket.Quantity);
            _ticketAdminHubContext.Clients.All.removeRegistrationHold(ticket.ConnectionId);

            // If all of the ticket registrations have completed, notify all clients in the queue that they are out of luck.
            if (_waitQueue.Any() && _registrationHolds.All(t => t.IsComplete))
            {
                _ticketHubContext.Clients.Clients(_waitQueue.Select(t => t.Item1).ToList()).outOfTickets();
            }
        }

        public void TimeOut(string connectionId)
        {
            // Just a simulation that can be called from the admin page
            var ticket = _registrationHolds.FirstOrDefault(t => t.ConnectionId == connectionId);
            if (ticket == null) return;

            lock (_registrationHolds)
            {
                // Remove the client from the registration holds and notify them that they have timed out. So sad.
                _registrationHolds.Remove(ticket);
                _ticketHubContext.Clients.Client(ticket.ConnectionId).timedOut();
                _ticketAdminHubContext.Clients.All.removeRegistrationHold(ticket.ConnectionId);
            }

            // Now that we have some tickets that freed up lets move through the wait queue until we don't
            while (_waitQueue.Any() && _quantity - _registrationHolds.Select(t => t.Quantity).Sum() > 0)
            {
                var nextInLine = _waitQueue.Dequeue();
                _ticketAdminHubContext.Clients.All.removeQueuedUpConnection(nextInLine.Item1);
                Register(nextInLine.Item1, nextInLine.Item2);
            }
        }

        public Ticket GetTicket(Guid ticketId)
        {
            return _registrationHolds.FirstOrDefault(t => t.RegistrationId == ticketId);
        }
    }
}
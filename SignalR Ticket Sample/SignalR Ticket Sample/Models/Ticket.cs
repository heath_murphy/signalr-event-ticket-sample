﻿using System;

namespace SignalRTicketSample.Models
{
    public class Ticket
    {
        public Guid RegistrationId { get; set; }
        public Guid EventId { get; set; }
        public string ConnectionId { get; set; }
        public int Quantity { get; set; }
        public bool IsComplete { get; set; }
    }
}
﻿using System;

namespace SignalRTicketSample.Models
{
    public class RegistrationInformation
    {
        public DateTime StartTime { get; set; }
    }
}
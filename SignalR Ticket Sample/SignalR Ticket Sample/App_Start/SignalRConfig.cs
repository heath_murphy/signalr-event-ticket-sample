﻿using Microsoft.Owin;
using Owin;
using SignalRTicketSample.App_Start;

[assembly: OwinStartup(typeof(Startup))]
namespace SignalRTicketSample.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }
}